import platform
from pathlib import Path


print('current path', Path('.').resolve())
print('current path absolute', Path('.').absolute())
print('home directory', Path().home())
print('cwd', Path().cwd())
Path('.', 'test').write_bytes(b'sucess')
print(platform.node())
print(platform.machine())
print(platform.system(), platform.release())
print(platform.uname())
print(platform.python_compiler())
print(platform.python_implementation(), platform.python_version())
print(platform.python_implementation(), platform.python_version())
